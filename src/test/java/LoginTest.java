import exceptions.UserValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import io.jsonwebtoken.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class LoginTest {
    /** Test Fixtures
     * setUp, login, state, users
     * */
    private Login login;
    private State state;
    private List<User> users;

    @BeforeEach
    public void setUp(){
        login = new Login();
        state = new State();
        users = new ArrayList<>(List.of(
                new User(1, "anna", "losen",  State.Role.ADMIN),
                new User(2, "berit", "123456", State.Role.TEACHER),
                new User(3, "kalle", "password", State.Role.STUDENT)));
    }

    /** No.1 - G - Login
     * HAPPY PATH:
     *  Check if username with password exists, fails if not
     * */
    @Test
    public void test_is_valid_username_and_password_success(){
        boolean result = login.isValidUser(users.get(0).getUsername(), users.get(0).getPassword());
        assertTrue(result);
    }

    /** No.2 - G - Authentication
     * Encoded username = (Base64) token
     * HAPPY PATHS:
     *  Check if user exist before generating token, fails if not.
     *  Check if token is authentic, fails if not.
     * */
    @ParameterizedTest
    @CsvSource(value = {"anna, losen", "berit, 123456", "kalle, password"})
    public void test_validate_user_token_success(String username, String password){
        String result = login.validateToken(username, password);
        assertEquals(login.encode(username), result);
    }

    @ParameterizedTest
    @ValueSource(strings = {"YW5uYQ==", "YmVyaXQ=", "a2FsbGU="})
    public void test_is_authentic_token_success(String token){
        boolean result = login.isAuthenticToken(token);
        assertTrue(result);
    }

    /** No.3 - VG - Authentication & Authorization
     * HAPPY PATHS:
     *  Check if user exist before generating JWT-token, then check if new jwt is authentic, fails if not.
     *  Check if user with jwt is authorized, to get correct permissions on specific domain, fails if not.
     *
     * UNHAPPY PATHS:
     *  Throws if username and role is mismatch (Generate new JWT)
     *  Throws if invalid jwt for valid user
     *  Throws if invalid jwt for permissions
     * */
    @ParameterizedTest
    @CsvSource(value = {"anna, losen", "berit, 123456", "kalle, password"})
    public void test_valid_user_new_jwt_is_authentic_success(String username, String password){
        String result = login.validateJWT(username, password);
        assertDoesNotThrow(() -> login.isAuthenticJWT(result));
    }

    @ParameterizedTest
    @CsvSource(value = {"anna, ADMIN", "berit, TEACHER", "kalle, STUDENT"})
    public void test_new_jwt_is_authentic_success(String username, State.Role role){
        String token = login.newJWT(username, role);
        assertDoesNotThrow(() -> login.isAuthenticJWT(token));
    }

    @ParameterizedTest
    @CsvSource(value = {"anna, ADMIN, GRADING", "berit, TEACHER, COURSE_FEEDBACK", "kalle, STUDENT, GRADING"})
    public void test_authenticate_user_domain_state_success(String username, State.Role role, State.Domain domain){
        String token = login.newJWT(username, role);
        List<String> result = login.authorizePermission(token, domain);
        assertIterableEquals(result, state.validateState(domain, role));
    }

    // Unhappy paths 
    @ParameterizedTest
    @CsvSource(value = {"bob, ADMIN", "beritt, TEACHER", " , STUDENT"})
    public void test_new_jwt_invalid_username_throws(String username, State.Role role){
        UserValidationException myIE = assertThrows(UserValidationException.class,
                () -> login.newJWT(username, role));
        assertEquals("Generate JWT for that user failed exception", myIE.getMessage());
    }

    @ParameterizedTest
    @CsvSource(value = {"anna, ADMIN", "berit, TEACHER", "kalle, STUDENT"})
    public void test_JWT_access_permission_state_throws(String username, State.Role role){
        String token = login.newJWT(username, role);
        SignatureException se = assertThrows(SignatureException.class,
                () -> login.isAuthenticJWT(token + "test_throw"));
        assertEquals("JWT signature does not match locally computed signature. JWT validity cannot be asserted and should not be trusted.", se.getMessage());
    }

    @Test
    public void test_authorize_users_role_throws(){
        IllegalArgumentException iae = assertThrows(IllegalArgumentException.class,
                () -> login.authorizePermission(
                        "", State.Domain.GRADING));
        assertEquals("JWT String argument cannot be null or empty.", iae.getMessage());
    }
}