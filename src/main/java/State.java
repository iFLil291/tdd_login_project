import exceptions.UserValidationException;

import java.util.List;

public class State {
    enum Role {
        ADMIN {
            @Override
            public boolean isAdmin() {
                return true;
            }
        },
        TEACHER {
            @Override
            public boolean isTeacher() {
                return true;
            }
        },
        STUDENT {
            @Override
            public boolean isStudent() {
                return true;
            }
        };

        Role() {
            isAdmin();
            isTeacher();
            isStudent();
        }
        public boolean isAdmin() {
            return false;
        }
        public boolean isTeacher() {
            return false;
        }
        public boolean isStudent() {
            return false;
        }
    }

    enum Domain {
        GRADING {
            @Override
            public boolean isGrading() {
                return true;
            }
        },
        COURSE_FEEDBACK {
            @Override
            public boolean isCourseFeedback() {
                return true;
            }
        };

        Domain() {
            isGrading();
            isCourseFeedback();
        }
        public boolean isGrading() {
            return false;
        }
        public boolean isCourseFeedback() {
            return false;
        }
    }

    public State(){}
    // Double check if the access-map request is authentic
    public List<String> validateState(Domain domain, Role role){
        switch (domain) {
            case GRADING -> {
                if (role.equals(State.Role.TEACHER)) {
                    return List.of("READ", "WRITE");
                }
                else if(role.equals(State.Role.ADMIN)){
                    return List.of("READ", "WRITE");
                }
                else if (role.equals(State.Role.STUDENT)){
                    return List.of("READ");
                }
                throw new UserValidationException("No permissions for that role on this domain exception");
            }
            case COURSE_FEEDBACK -> {
                if (role.equals(State.Role.STUDENT)) {
                    return List.of("READ", "WRITE");
                }
                else if(role.equals(State.Role.ADMIN)){
                    return List.of("READ", "WRITE");
                }
                else if (role.equals(State.Role.TEACHER)){
                    return List.of("READ");
                }
                throw new UserValidationException("No permissions for that role on this domain exception");
            }
            default -> throw new UserValidationException("Requested permission invalid exception");
        }
    }
}
