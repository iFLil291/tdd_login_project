import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import exceptions.UserValidationException;
import utils.Secret;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Login {
    /**
     * JWT-specific object
     *  String value - fetched from class Secret.java (excluded from GitLab)
     *  byte[] bytes - value^ encoded to bytes with Base64.getEncoder().encode
     *  SecretKey key - bytes^ used for SecretKeySpec with HmacSHA256
     */
    private final Secret service = new Secret();

    private final List<User> users = new ArrayList<>(List.of(
            new User(1, "anna", "losen", State.Role.ADMIN),
            new User(2, "berit", "123456", State.Role.TEACHER),
            new User(3, "kalle", "password", State.Role.STUDENT)
    ));

    /**
     * No.1 - G - Login
     * validate user by name and password
     */
    public boolean isValidUser(String username, String password) {
        for (User user : users) {
            if (username.equals(user.getUsername()) && password.equals(user.getPassword())) {
                return true;
            }
        }
        throw new UserValidationException("Username or Password mismatch exception");
    }

    /**
     * No.2 - G - Authenticate with token
     * If user is valid -> give encoded username.
     * If username match incoming decoded token it is authentic.
     */
    public String validateToken(String username, String password) {
        if (isValidUser(username, password)) {
            return encode(username);
        }
        return new UserValidationException("validateUser -> isValidUser is false exception").toString();
    }

    public boolean isAuthenticToken(String token) {
        token = decode(token);
        for (User user : users) {
            if (token.equals(user.getUsername())) {
                return true;
            }
        }
        throw new UserValidationException("Token is not valid exception");
    }

    public String encode(String value) {
        byte[] token = value.getBytes();
        return Base64.getEncoder().encodeToString(token);
    }

    private String decode(String token) {
        byte[] value = Base64.getDecoder().decode(token);
        return new String(value);
    }

    /**
     * No.3 - VG - Authentication & Authorization
     * Check if each user with username and password is valid, then ask to generate JWT-token with role, else throw.
     * Check if each user with username and role is valid, then generate JWT-token, else throw.
     * Check if jwt is authentic, else throw SignatureException.
     * Check if claims from token matches for each user by username & role, return access-map for each role, else throws.
     */
    public String validateJWT(String username, String password) {
        if (isValidUser(username, password)) {
            for (User user : users) {
                return newJWT(user.getUsername(), user.getRole());
            }
        }
        return new UserValidationException("Username invalid exception").toString();
    }

    public String newJWT(String username, State.Role role) {
        for (User user : users) {
            if (user.getUsername().equals(username) && user.getRole().equals(role)){
                return Jwts.builder()
                        .setIssuer(service.getIssuer())
                        .setSubject(username)
                        .setIssuedAt(Date.from(Instant.now()))
                        .setExpiration(Date.from(Instant.now().plus(5, ChronoUnit.MINUTES)))
                        .claim("role", role)
                        .signWith(service.getKey())
                        .compact();
            }
        }
        throw new UserValidationException("Generate JWT for that user failed exception");
    }

    public boolean isAuthenticJWT(String token) {
        Jwts.parserBuilder()
                .setSigningKey(service.getKey())
                .build()
                .parseClaimsJws(token);
        return true;
    }

    public List<String> authorizePermission(String token, State.Domain domain){
        Map<State.Domain, List<String>> access = new HashMap<>();

        Claims claims = Jwts.parserBuilder()
                .setSigningKey(service.getKey())
                .build()
                .parseClaimsJws(token)
                .getBody();

        String sub = claims.getSubject();
        String role = claims.get("role", String.class);

        for (User user : users) {
            if (user.getUsername().equals(sub)) {
                if (role.equals(State.Role.ADMIN.toString())) {
                    access.put(State.Domain.GRADING, List.of("READ", "WRITE"));
                    access.put(State.Domain.COURSE_FEEDBACK, List.of("READ", "WRITE"));
                    return access.get(domain);
                }
                else if(role.equals(State.Role.TEACHER.toString())){
                    access.put(State.Domain.GRADING, List.of("READ", "WRITE"));
                    access.put(State.Domain.COURSE_FEEDBACK, List.of("READ"));
                    return access.get(domain);
                }
                else if(role.equals(State.Role.STUDENT.toString())){
                    access.put(State.Domain.GRADING, List.of("READ"));
                    access.put(State.Domain.COURSE_FEEDBACK, List.of("READ", "WRITE"));
                    return access.get(domain);
                }
                else {
                    throw new UserValidationException("User and Role mismatch exception");
                }
            }
        }
        throw new IllegalArgumentException();
    }
}