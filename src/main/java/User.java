public class User {

    private final int id;
    private final String username;
    private final String password;
    private final State.Role role;

    public User(int id, String username, String password, State.Role role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public State.Role getRole() {
        return role;
    }
}
