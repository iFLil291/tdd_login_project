#Assignment
### Reflections
If I were to redo the assignment I would have used a DTO or DAO pattern.
I would have reconstructed by take out the token parts and make them to helpers/utils with singelton pattern for reusable causes.
I could also assign my User object a token/jwt, like I did with role.  

I could also have a different approach with data models of Role (instead of a State.enum) and User, using a mapper to connect the two with LinkedLists<>. 
The tricky part about the project was mostly the new experience of thinking how to write code from a different perspective. 

Another improvement I could have done would be to clean up the code and not continue with new after each part of the assignment. 
Instead, I chose to fill out with new test-methods and new code along the way.

